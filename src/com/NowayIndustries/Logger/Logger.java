package com.NowayIndustries.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
/**
 * @author NowayIndustries
 * Name: Logger.java
 * Date: 10-09-2014 (DD-MM-YYYY)
 * Creator: NowayIndustries
 * Purpose: A universal logging library/singleton, with support for different levels of detail. Don't forget to set productname before logging anything!
 */

public class Logger {
    private static Logger instance = new Logger();
    private String productName = "log";
    public enum LoggingLevel {DEBUG, VERBOSE, ERROR, NORMAL};
    private int lengthOfLongestLoggingLevel = 0; //Used for dynamically padding the logginglevel strings.
    private LoggingLevel currentLoggingLevel = LoggingLevel.VERBOSE;

    private enum Status {SETUP, IDLE, WRITING}
    private Status currentStatus = Status.SETUP;

    private boolean useSplitLogfiles;
    private File logFile;
    private File normalFile;
    private File errorFile;
    private File verboseFile;
    private File debugFile;

    private Logger() {
        this.currentStatus = Status.SETUP;
        this.useSplitLogfiles = false; //default to unsplit log files, for backwards compatability.
    }

    private void openLogFile() {
        try{
            logFile = new File(this.productName +getDateStamp()+ ".log");
            if(!logFile.exists())
                logFile.createNewFile();
        }
        catch(Exception e)
        {
            System.out.println("[Logger] Unable to open logfile!");
        }
    }
    private void openNormalFile() {
        try{
            normalFile = new File(this.productName +getDateStamp()+ ".normal");
            if(!normalFile.exists())
            	normalFile.createNewFile();
        }
        catch(Exception e)
        {
            System.out.println("[Logger] Unable to open normalfile!");
        }
    }
    private void openErrorFile() {
        try{
        	errorFile = new File(this.productName +getDateStamp()+ ".error");
            if(!errorFile.exists())
            	errorFile.createNewFile();
        }
        catch(Exception e)
        {
            System.out.println("[Logger] Unable to open errorfile!");
        }
    }
    private void openVerboseFile() {
        try{
        	verboseFile = new File(this.productName +getDateStamp()+ ".verbose");
            if(!verboseFile.exists())
            	verboseFile.createNewFile();
        }
        catch(Exception e)
        {
            System.out.println("[Logger] Unable to open verbosefile!");
        }
    }private void openDebugFile() {
        try{
            debugFile = new File(this.productName +getDateStamp()+ ".debug");
            if(!debugFile.exists())
                debugFile.createNewFile();
        }
        catch(Exception e)
        {
            System.out.println("[Logger] Unable to open debugfile!");
        }
    }

    private class UnconfiguredLoggerException extends RuntimeException
    {
        public UnconfiguredLoggerException(){
            super("Logger has not been configured yet! (setProductName)");
        }
    }

    /**
     * Makes it so that the logginglevel text is same length (padded with spaces at end of logginglevel)
     * @param msgLevel What logginglevel is the message?
     * @return Padded string for msgLevel
     */
    private String getPaddedMessageLevel(LoggingLevel msgLevel){
    	if(this.lengthOfLongestLoggingLevel == 0){
    		for(LoggingLevel l : LoggingLevel.values()){
    			if(l.toString().length() > this.lengthOfLongestLoggingLevel){
    				this.lengthOfLongestLoggingLevel = l.toString().length();
    			}
    		}
    	}
		int paddingLength = this.lengthOfLongestLoggingLevel - msgLevel.toString().length();
		String padding = "";
		for(int i = 0; i < paddingLength; i++){
			padding += " ";
		}
		return msgLevel.toString() + padding;
    }
    /**
     * @return Current instance of this class.
     */
    public static Logger getInstance() {
        return instance;
    }

    /**
     * Changes the name used for logfilename.
     * @param productName Name of the product that the log is for.
     */
    public void setProductName(String productName) {
        this.productName = productName;
        this.openLogFile();
        if(this.useSplitLogfiles){
        	this.openErrorFile();
        	this.openVerboseFile();
        	this.openDebugFile();
        }
        this.currentStatus = Status.IDLE;
    }
    /**
     * Enables the use of split log files. This means the different logginglevels have their own logfiles:<br>
     * NORMAL: .normal<br>
     * ERROR: .error<br>
     * VERBOSE: .verbose<br>
     * DEBUG: .debug<br>
     * <br>
     * These split files ONLY contain messages of THEIR OWN logging level.<br>
     * The .log file is a copy of the terminal, and thus contains messages depening on the logginglevel set (as it worked before).<br>
     * NORMAL - Only informational messages are shown.<br>
     * ERROR - Only errors are shown.<br>
     * VERBOSE - Show additional log (Also shows NORMAL & ERROR messages).<br>
     * DEBUG - All messages are shown.<br>
     */
    public void useSplitLogfiles(){
    	this.useSplitLogfiles = true;
    	this.openNormalFile();
    	this.openErrorFile();
    	this.openVerboseFile();
    	this.openDebugFile();
    	
    }

    /**
     * Changes the logging level to change the logging detail level. Messages will be written to file and to STDOUT.
     * NORMAL - Only informational messages are shown.<br>
     * ERROR - Only errors are shown.<br>
     * VERBOSE - Show additional log (Also shows NORMAL & ERROR messages).<br>
     * DEBUG - All messages are shown.<br>
     * @param newLevel Logging level to change to.
     * @default VERBOSE
     */
    public void setLoggingLevel(LoggingLevel newLevel) {
        this.log(LoggingLevel.DEBUG, "Logger", "Changing from '" + this.currentLoggingLevel + "' to '" + newLevel + "' logging level.");
        this.currentLoggingLevel = newLevel;
    }

    /**
     * Provides you with a full timestamp.
     * @return The full timestamp for example: '[Wed Sep 18 10:26:04 CEST 2013]'.
     */
    public String getFullTimeStamp()
    {
        Calendar calendar = new GregorianCalendar();
        return calendar.getTime().toString();
    }

    /**
     * Returns a time only timestamp.
     * @return The timestamp, for example '[10:26:04]'.
     */
    public String getTimeStamp()
    {
        Calendar calendar = new GregorianCalendar();
        String hour = ""+calendar.get(Calendar.HOUR_OF_DAY);
        String minute = ""+calendar.get(Calendar.MINUTE);
        String second = ""+calendar.get(Calendar.SECOND);

        if(hour.length() == 1)
            hour = "0"+hour;
        if(minute.length() == 1)
            minute = "0"+minute;
        if(second.length() == 1)
            second = "0"+second;

        return hour + ":" + minute + ":" + second;
    }

    /**
     * Creates a datestamp in the format 'Year-Month-Day'.
     * @return The datestamp
     */
    public String getDateStamp() {
        Calendar calendar = new GregorianCalendar();
        String day = ""+calendar.get(Calendar.DAY_OF_MONTH);
        String month = ""+calendar.get(Calendar.MONTH);
        String year = ""+calendar.get(Calendar.YEAR);

        if(day.length() == 1)
            day = "0"+day;
        if(month.length() == 1)
            month = "0"+month;
        if(year.length() == 1)
            year = "0"+year;

        return year + "-" + month + "-" + day;
    }

    /**
     * Logs message, with a prefix and certain logginglevel.
     * @param msgLevel Logginglevel of the message.
     * @param prefix String used for identification of message origin
     * @param msg Actual message to be logged.
     */
    private void log(LoggingLevel msgLevel, String prefix, String msg) {
        if(this.currentStatus == Status.SETUP)
            throw new UnconfiguredLoggerException();
        this.currentStatus = Status.WRITING;
        boolean outputMsg = false;
        switch(this.currentLoggingLevel) {
            case NORMAL:
                switch (msgLevel) {
                    case NORMAL:
                        outputMsg = true;
                        break;
                }
                break;
            case ERROR:
                switch (msgLevel) {
                    case ERROR:
                        outputMsg = true;
                        break;
                }
                break;
            case VERBOSE:
                switch (msgLevel) {
                    case NORMAL:
                    case ERROR:
                    case VERBOSE:
                        outputMsg = true;
                        break;
                }
                break;
            case DEBUG:
            default:
                outputMsg = true;
                break;
        }
        if(outputMsg){
            String out = "[" +this.getTimeStamp()+ "][" +getPaddedMessageLevel(msgLevel)+ "][" +prefix+ "] " +msg;
            System.out.println(out);
            try{
                BufferedWriter logWriter = new BufferedWriter(new FileWriter(logFile, true));
                logWriter.write(out);
                logWriter.newLine();
                logWriter.close();
            }catch(Exception e){
                System.out.println("[Logger] Unable to write to file!");
                e.printStackTrace();
            }
        }
        if(this.useSplitLogfiles){
        	String out = "[" +this.getTimeStamp()+ "][" +getPaddedMessageLevel(msgLevel)+ "][" +prefix+ "] " +msg;
        	File loggingLevelFile;
        	switch(msgLevel){
        	case NORMAL:
        		loggingLevelFile = this.normalFile;
        		break;
        	case ERROR:
        		loggingLevelFile = this.errorFile;
        		break;
        	case VERBOSE:
        		loggingLevelFile = this.verboseFile;
        		break;
        	default: //Prevents loggingLevelFile not initiated error.
        		loggingLevelFile = this.debugFile;
        		break;
        	}
            try{
                BufferedWriter logWriter = new BufferedWriter(new FileWriter(loggingLevelFile, true));
                logWriter.write(out);
                logWriter.newLine();
                logWriter.close();
            }catch(Exception e){
                System.out.println("[Logger] Unable to write to " +msgLevel.toString()+ "file!");
                e.printStackTrace();
            }
        }
        this.currentStatus = Status.IDLE;
    }

    /**
     * Shortcut for logging debug information.
     * @param prefix Used to indicate what class this log entry came from. (Make it descriptive)
     * @param msg Actual message to be logged.
     */
    public void debug(String prefix, String msg){
        this.log(LoggingLevel.DEBUG, prefix, msg);
    }

    /**
     * Shortcut for error messages.
     * @param prefix Used to indicate what class this log entry came from. (Make it descriptive)
     * @param msg Actual message to be logged.
     */
    public void error(String prefix, String msg){
        this.log(LoggingLevel.ERROR, prefix, msg);
    }

    /**
     * Schortcut for information logging.
     * @param prefix Used to indicate what class this log entry came from. (Make it descriptive)
     * @param msg Actual message to be logged.
     */
    public void log(String prefix, String msg){
        this.log(LoggingLevel.NORMAL, prefix, msg);
    }

    /**
     * Shortcut for logging additional information.
     * @param prefix Used to indicate what class this log entry came from. (Make it descriptive)
     * @param msg Actual message to be logged.
     */
    public void info(String prefix, String msg){
        this.log(LoggingLevel.VERBOSE, prefix, msg);
    }
}
