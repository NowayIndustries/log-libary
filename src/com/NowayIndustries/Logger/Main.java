package com.NowayIndustries.Logger;

/**
 * @author NowayIndustries
 * Name: Main.java (Logger)
 * Date: 10-09-2014 (DD-MM-YYYY)
 * Creator: NowayIndustries
 * Purpose: A quick testcase class for use with the Logger library/singleton.
 */
public class Main {

    public static void main(String[] args) {
        //Logger.getInstance().log(Logger.LoggingLevel.NORMAL, "MAIN", "Testing unconfigured logger.");
        System.out.println("Working Directory = " + System.getProperty("user.dir"));
        Logger.getInstance().setProductName("Logger");

        Logger.getInstance().setLoggingLevel(Logger.LoggingLevel.DEBUG);

        Logger.getInstance().setLoggingLevel(Logger.LoggingLevel.NORMAL);
        testLogs(Logger.getInstance());

        Logger.getInstance().setLoggingLevel(Logger.LoggingLevel.ERROR);
        testLogs(Logger.getInstance());

        Logger.getInstance().setLoggingLevel(Logger.LoggingLevel.VERBOSE);
        testLogs(Logger.getInstance());

        Logger.getInstance().setLoggingLevel(Logger.LoggingLevel.DEBUG);
        testLogs(Logger.getInstance());
    }

    private static void testLogs(Logger l){
        l.log("MAIN", "Normal msglogging level");
        l.error("MAIN", "Error msglogging level");
        l.info("MAIN", "Verbose msglogging level");
        l.debug("MAIN", "Debug msglogging level");
    }
}
