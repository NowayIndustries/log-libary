# LoggingLibrary by NowayIndustries #

This is my logginglibrary which supports different logginglevels and uses the singleton design pattern.
### How do I use this? ###

1. Link the jar file in your project.
2. Set the productname (Logger.getInstance().setProductName("Example");)
3. Optionally enable split logfiles (Logger.getInstance.useSplitLogfiles();)
4. Start using it! (Logger.getInstance().log("PREFIX", "MESSAGE");)

* Use .log for normal information messages.
* Use .error for error messages.
* Use .verbose for additional information.
* Use .debug for debug messages.

### What does X mean? ###
***LoggingLevel***

LoggingLevel refers to the 'amount' of logging that is done by the library.

***Split logfiles***

This refers to the use of seperate logfiles for the different logginlevels in addition to the main logfile.
These split logfiles ONLY contain the messages of their own logginglevels. (Their extension is the logging level: .normal, .error, .verbose, .debug)

The logfile (and console) contain messages relative to the level of logging the library is set to.

**NORMAL:** Only informational messages are shown.

**ERROR:** Only errors are shown.

**VERBOSE:** Show additional log (Also shows NORMAL & ERROR messages).

**DEBUG:** All messages are shown.


### Special thanks to ###
* aal89 - Peer reviewer

